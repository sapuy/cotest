function updatePrice(carInsurance) {

  let response = [];
  carInsurance.products.forEach(element => {
    let reduce = element.sellIn <= 0 ? 2 : 1;

    switch (element.name) {
      case 'Full Coverage':
        element.price = element.price + reduce;//actually increases in price the older it gets
        element = validatePrice(element);
        element.sellIn--;
        break;

      case 'Mega Coverage':
        //Do nothing
        break;
      case 'Special Full Coverage':

        if (element.sellIn > 10) {

          element.price = element.price + 1;
        }
        else if (element.sellIn <= 10 && element.sellIn > 5) {
          element.price = element.price + 2;

        }
        else if (element.sellIn <= 5 && element.sellIn > 0) {
          element.price = element.price + 3;

        }
        else if (element.sellIn <= 0) {
          element.price = 0;
        }
        element = validatePrice(element);
        element.sellIn--;
        break;
      case 'Super Sale':

        element.price = element.price - (reduce * 2);
        element = validatePrice(element);
        element.sellIn--;

        break;
      default:

        element.price = element.price - reduce
        element = validatePrice(element);
        element.sellIn--;
        break;
    }


    response.push(element);

  });
  return response;


}

function validatePrice(element) {
  if (element.price < 0) {
    element.price = 0;
  } else {

    const limit = 50;
    element.price = element.price > limit ? limit : element.price;
  }
  return element;
}


module.exports = {
  updatePrice
}
