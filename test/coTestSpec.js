const expect = require('chai').expect;

const coTest = require('../src/controllers/coTest');
const CarInsurance = require('../src/models/CarInsurance').CarInsurance;
const Product = require('../src/models/Product').Product;

describe("Co Test", function() {

  it("should 19", function() {
    const insurance = new CarInsurance([ new Product('Special Full Coverage', 10, 49) ]);
    console.log("Product",insurance.products[0]);
    const products = coTest.updatePrice(insurance);
    console.log("Product Result",products[0]);
    expect(products[0].price).equal(50);
  });

});
